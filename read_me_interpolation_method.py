from scipy.interpolate import RegularGridInterpolator
import gzip
import numpy as np

lumbins=np.linspace(41,47,150)
zbin1=np.arange(0.002,0.1,0.005)
zbin2=np.arange(0.1,1.0,0.05)
zbin3=np.arange(1.0,5.05,0.05)
zbins=np.concatenate([zbin1,zbin2,zbin3]) 
nHbins=np.linspace(20,26,80)

#read in matrix:
reading_gzip = gzip.GzipFile("final_sol_all.npy.gz", "r")
loading_matr=np.load(reading_gzip)

lum_func=RegularGridInterpolator((zbins, lumbins, nHbins), 
							loading_matr[0], method='linear',
							bounds_error=False, fill_value=0)

#To get space densities, use lum_func([redshift,Lum_2_10_keV,log_nH])
#the input can be an array of n elements, where each 
#element has the format [redshift,Lum_2_10_keV,log_nH]

